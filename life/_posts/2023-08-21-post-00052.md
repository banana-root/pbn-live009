---
layout: post
title: "7.10 (월) '시흥 롯데캐슬 시그니처' 청약 정보 - 주변 입지 정보, 청약 정보 (청약 계획, 평면도, 중도금 등)"
toc: true
---


## '시흥 롯데캐슬 시그니처' 청약 정보 (2023.07.10 ~ 2023.07.12 예정)
 안녕하세요. 청악 인사이더입니다.
 

 7월 10일은 ‘청량리 롯데캐슬 하이루체’와 더불어 ‘시흥 롯데캐슬 시그니처’의 청약일이기도 합니다.

 

 시흥 롯데 캐슬의 완공일은 2027년 7월 예정되어,
 2023년 7월 10일에 청약을 진행한 이다음 당첨자는 7월 19일과 20일에 계획되어 있습니다.
 

 이번 시간에는 시흥 롯데캐슬 시그니처의 주변 뒷배경 및 청약 정보에 대해서 알아보겠습니다.

### I. 근방 환경

#### 1. 주소지

 경기도 시흥시 검바위1로 59-6
 

#### 2. 교통 환경
 시흥 롯데캐슬 시그니처에서 발길 약 15분 거리, 대중교통으로는 약 10분 거리에 서해선 신천역이 일자리 잡고 있습니다.
 신천역에서 소사역으로 8분만 이동하면, 한가운데 지하철 1호선으로 환승할 생목숨 있고,
 25분가량 타고 초지역으로 이동하면 4호선으로 환승할 생목숨 있어,
 수도권 서남부에 위치한 직장에 출퇴근 노래 상당한 이점이 있을 것으로 생각됩니다.
 

#### 3. 구수 사방 (지도 내 주황색 깃발 표시)
 시흥 롯데캐슬 시그니처 근처에는 많은 초등학교가 있는 것이 특징이라고 할 목숨 있습니다.
 - 초등학교 : 은빛초등학교, 검바위초등학교, 신일초등학교, 웃터골초등학교, 금모래초등학교, 소래초등학교, 신천초등학교
 - 중학교 : 소래중학교, 신천중학교
 -고등학교 : 소래고등학교, 은행고등학교, 신천고등학교
 

 초등학교부터 중학교, 고등학교가 근처에 있어 자녀들이 도보로 통학할 행우 있는 장점이 있겠습니다.
 

 

#### 4. 자연환경 (지도 눈치 초록색 나무 표시)
 교육뿐만 아니라, 산책이나 운동을 즐겨 할 [시흥롯데캐슬](https://plan-housing.co.kr) 복수 있는 이른바 ‘공세권’도 청약하시는 분들이 무진히 생각하는 부분인데요,
 이런 면에서는 시흥 롯데캐슬이 보이는 대표적인 장점이 되지 않을까 싶습니다.
 

 공원으로는 발걸음 30분 내로, 은계 중앙공원, 검바위 하늘공원, 은행천 물길공원, 신일공원, 비둘기 공원, 백제공원, 복음공원, 신천 근린공원 외에 약 25곳 이상의 공원을 갖고 있습니다.
 

 자연환경과 어우러져 있는 곳에 지점 잡은 특징이 있네요. 가족이나 연인과 함께 다양한 곳에서 산책과 운동을 할 성명 있는 것도 장점이라고 생각됩니다.
 

#### 5. 마트/시장 (롯데마트 - 정상 냄새 노란색 표현 , 하나로마트 / 스타필드 부천 / 소래포구 어시장 - 미표시)
 시흥 롯데캐슬 시그니처와 쥔아저씨 가까운 순부터 차례로 찾아보도록 하겠습니다.
 

 앞서 차로 7분 거리에 롯데마트가 있어 필요한 공산품이나 생필품들을 물고기 이운 있습니다.
 

 또한 차로 10분거리에는 하나로마트 북시흥농협 본점이 있어, 싱싱한 우리 농/수/축산물을 구입할 수명 있겠습니다.
 

 차로 20분 거리에는 복합 쇼핑몰 스타필드 부천점이 있어, 데카트론, 일렉트로마트, 메가박스 등 다양한 쇼핑을 즐길 생명 있는 것도 장점이라고 생각됩니다.
 

 그뿐만 아니라 K-코스트코라고 불리는 이마트 트레이더스 홀세일 클럽도 스타필드 부천점에 있기 왜냐하면 롯데마트나 하나로마트와는 당분간 다른 제품들을 쉽게 만나볼 호운 있겠습니다.
 

 아울러 차로 20분 거리에 소래포구 종합어시장이 있어, 싱싱한 수산물들을 쉽게 전일 핵 있다는 것이 장점이 되겠습니다.
 

#### 6. 보건소 (지도 내 빨간색 십자가 표시)
 청약 애오라지 아파트에 병원이 있는 것은 원체 중요하게 여겨집니다.
 응급한 상황이 발생할 시, 의료 등 다양한 이점이 있기 때문입니다.
 시흥 롯데캐슬 시그니처 근처에는 차로 5분 거리에 위치한 신천연합병원이 있습니다.
 신천 연합병원은 총 15여 수심 진료과가 있고 그중에서는 내과, 외과, 정형외과, 부인과뿐만 아니라 요즈음 진료받기 힘든 소아청소년과가 있습니다.
 게다가 응급실도 있어서 응급 환자가 발생할 때도 빠르게 조치가 가능할 것으로 생각됩니다.

### II. 청약 정보
 

 다음은 ‘시흥 롯데캐슬 시그니처’의 청약 정보에 대해 알아보겠습니다.
 

#### 1. 청약 지급 세대
 ‘시흥 롯데캐슬 시그니처’는 우선, 블록 단위로 나뉘게 됩니다.
 1블록과 2블록, 위치상으로는 하물며 옆이지만 지출 세대 생목숨 및 가격이 수유간 상이한 점이 특징입니다.
 

 - 1블록 : 일반공급 622세대와 특별공급 608세대가 공급될 예정, 총 1230 세대
 - 2블록 : 일반공급 462세대와 특별공급 441세대가 공급될 예정, 총 903 세대

#### 

#### 2. 청약 날짜, 유형
 1블록과 2블록 전부 청약 일자는 동일합니다.
 특별공급은 2023년 7월 10일 월요일에 진행되며, 1순위는 2023년 7월 11일 화요일, 2순위는 2023년 7월 12일 수요일에 청약 일정이 예고되어 있어, 해당하는 순위에 맞게 청약 접수를 하시면 되겠습니다.
 

 청약 발표일은 1블록의 세월 2023년 7월 19일에, 그리고 2블록의 탄원 2023년 7월 20일에 예정되어 있습니다.
 

 요번 청약에는 1블록과 2블록 청약 일자는 같지만, 청약 발표일이 다른데요,
 벅벅이 알아야 하실 점은 1블록과 2블록 중복 청약이 가능합니다.
 

 마침내 ‘시흥 롯데캐슬 시그니처’에 청약을 넣고자 하시는 분은 1블록과 2블록 모든 방을 살펴보시고,
 마음에 드시는 방의 구조, 청약 지발 세대 수, 경쟁률 등 다양한 항목들을 뻔쩍하면 따져 1블록과 2블록 전부 청약하시는 것이 좋겠습니다.
 (같은 블록 내에서의 중복 청약은 아내 됩니다.)
 

 차회 특별공급의 정세 청년이나 이전기관 및 여타 특공 대상은 계획에 없고,
 다자녀가구, 신혼부부, 생애최초, 노부모부양, 기관추천으로 총 5가지 대상으로 계획되어 있습니다.
 

 

#### 3. 면적
 ‘시흥 시그니처 롯데캐슬’의 공급되는 면적은 모조리 84형(환산 평수 36~37평의 면적)입니다.
 4인 가구나 식자 계획이 있는 신혼부부가 살기 좋은 평수입니다.
 그뿐만 아니라 84평은 부동산 시장에서 선호도가 높은 면적이기 때문에, 당첨 추후 임대나 매매할 적기 상당한 이점이 되겠습니다.
 

 공급 금액을 살펴보겠습니다.

 1블록의 애걸 최고가 표점 84A와 84B 타입은 7억 2천 5백만 원, 더구나 84C 타입은 7억 1천 8백만 원입니다.
 2블록의 이유 84A와 84B 타입은 7억 1천 3백만 원, 그리고 84C 타입은 7억 6천만 원입니다.
 

 계약금은 10%인데요, 내약 운문 3천만 원의 계약금을 납부한 뒤 1개월 낌새 차액을 납부하는 방식입니다.
 중도금 60%, 그리고 입주 단시 30%를 납부하게 됩니다.
 중도금 납부 계획에 대한 날짜는 부임 표에 기재되어 있으니 참고하시면 좋겠습니다.
 

 - 주택별 평면도
 

 다음은 주택별 평면도를 확인하겠습니다.

 84A, 84B, 84C의 구조는 타입마다 1블록과 2블록 전면 동일합니다.
 (예시: 1블록의 84A형과 2블록의 84A형은 전면 동일한 구조)
 

 반면에 방지 면적, 여개 공용 면적, 입약 면적이 피차 상이하니, 상세한 점은 아래의 표를 참고하시어 청약 계획에 도움이 되면 좋겠습니다.

#### 

#### 

#### 4. 영여 세부사항
 

 1) 동일 한개 냄새 특별공급과 일반공급에 대해 과정 1건씩 청약이 가능합니다. 여혹 특별공급 당첨이 되면 통상 공급에서는 제외됩니다.
 

 2) 시흥시에 거주하거나, 경기도 및 수도권에 거주하는 만 19세 이상인 계획 경우 청약이 가능합니다. 그러니 만 19세 이상의 합절 권솔 전야 참여해서 1블록과 2블록 송두리 청약하면 당첨 확률을 늘릴 요행 있겠습니다.
 

 3) ‘시흥 롯데캐슬 시그니처’는 비투기과열지구와 비청약과열지구에 해당합니다. 그러면 1주택 결료 소유하거나, 소유한 세대에 속한 분도 1순위 자격으로 청약할 목숨 있는 자격이 주어집니다.
 

 4) 분양가 상한제 미적용 주택입니다.
 

 5) 기존에 당첨 사실이 있어도 재당첨에 대한 제한이 없고 청약이 가능합니다.
 

 6) 도리어 당첨된 요다음 5년 가운데 투기 과열 지구와 청약 과열 지역에 대한 독립주택 청약에서 1순위 청약이 제한됩니다.
 

 7) 당첨 차기 전매 제한은 1년, 반면에 실거주 의무는 없습니다. 그렇기 그렇게 준공 뒤 임대를 줄 길운 있겠습니다.
 

 8) 견본 거주 소재지 : 경기도 시흥시 배곧동 270
     사이버 모범 하우스 바로가기 : https://www.lottecastle.co.kr/APT/AT00393/14218/vr/view.do
 이렇게 7월 10일부터 7월 12일까지 진행될 ‘시흥 롯데캐슬 시그니처’의 청약 정보에 대해 알아보았습니다.
 

 제호 글이 작게나마 청약에 도움이 되었으면 좋겠습니다.
 

 이상으로 포스팅을 마칩니다.
 

 자료 사진 출처 - https://www.lottecastle.co.kr/APT/AT00393/14032/eyeview/view.do
 

 저작권법에 의해 보호되는 저작물로서 이에 대한 무단 복제 및 배포를 금합니다.
